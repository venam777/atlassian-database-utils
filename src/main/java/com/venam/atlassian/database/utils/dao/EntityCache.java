package com.venam.atlassian.database.utils.dao;

import net.java.ao.Entity;

import java.util.*;

/**
 *
 */
public class EntityCache<T extends Entity> {

    private List<T> createdEntities = new LinkedList<>();
    private List<T> updatedEntities = new LinkedList<>();
    private Set<Integer> deletedEntities = new LinkedHashSet<>();

    EntityCache() {}

    public void clear() {
        createdEntities.clear();
        updatedEntities.clear();
        deletedEntities.clear();
    }

    public void create(T entity) {
        createdEntities.add(entity);
    }

    public void update(T entity) {
        updatedEntities.add(entity);
    }

    public void delete(Integer id) {
        deletedEntities.add(id);
    }

    public List<T> getCreatedEntities() {
        return Collections.unmodifiableList(createdEntities);
    }

    public List<T> getUpdatedEntities() {
        return Collections.unmodifiableList(updatedEntities);
    }

    public Set<Integer> getDeletedEntities() {
        return Collections.unmodifiableSet(deletedEntities);
    }

}
