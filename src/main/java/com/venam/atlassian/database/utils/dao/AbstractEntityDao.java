package com.venam.atlassian.database.utils.dao;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.google.common.base.CaseFormat;
import com.venam.atlassian.database.utils.tools.Converter;
import com.venam.atlassian.database.utils.tools.Filter;
import com.venam.atlassian.database.utils.tools.ReflectionUtils;
import com.venam.atlassian.database.utils.tools.StringUtils;
import net.java.ao.*;
import net.java.ao.types.TypeInfo;
import net.java.ao.types.TypeManager;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Родитель для всех DAO-классов. Содержит в себе как одиночные CRUD операции, так и массовые
 * Так же может работать с кешем (примитивным, но все же)
 */
public abstract class AbstractEntityDao<T extends Entity> implements EntityDao<T> {

    /**
     * Хранит в себе метаинформацию об entity: поля, название таблицы, entityManager
     */
    private class EntityInfo {

        private EntityManager entityManager;
        private String tableName;
        private TypeInfo<Integer> idType;
        private Map<String, TypeInfo<Integer>> fields = new LinkedHashMap<>();
        private Map<String, String> dbFields = new HashMap<>();
        private Map<String, Method> accessors = new HashMap<>();
        private Map<String, Method> mutators = new HashMap<>();

        EntityInfo(Class<T> entityClass, T entity) throws ReflectiveOperationException {
            this.entityManager = entity.getEntityManager();
            this.tableName = getTableName(entityClass);
            TypeManager typeManager = entityManager.getProvider().getTypeManager();
            for (Method method : entityClass.getMethods()) {
                if (!Modifier.isStatic(method.getModifiers())) {
                    if (method.getName().startsWith("set")) {
                        String fieldName = StringUtils.deCapitalize(method.getName().substring("set".length()));
                        Class javaType = method.getParameterTypes()[0];
                        fields.put(fieldName, typeManager.getType(javaType));
                        if (method.isAnnotationPresent(Mutator.class)) {
                            Mutator mutator = method.getAnnotation(Mutator.class);
                            dbFields.put(fieldName, mutator.value());
                        } else if (Entity.class.isAssignableFrom(javaType)) {
                            dbFields.put(fieldName, getPolymorphicName(fieldName + "Id"));
                        } else {
                            dbFields.put(fieldName, getPolymorphicName(fieldName));
                        }
                        mutators.put(fieldName, method);
                    } else if (method.getName().startsWith("get") || method.getName().startsWith("is")) {
                        String fieldName = StringUtils.deCapitalize(method.getName().substring(method.getName().startsWith("get") ? "get".length() : "is".length()));
                        accessors.put(fieldName, method);
                    }
                }

            }
            idType = typeManager.getType(Integer.class);
        }

        public Map<String, TypeInfo<Integer>> getFields() {
            return fields;
        }

        public String getDbFieldName(String fieldName) {
            //без кавычек в POSTGRESQL не будет работать
            return "\"" + dbFields.get(fieldName) + "\"";
        }

        public EntityManager getEntityManager() {
            return entityManager;
        }

        public String getTableName() {
            return tableName;
        }

        public TypeInfo<Integer> getIdType() {
            return idType;
        }

        private String getTableName(Class<T> entityClass) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
            //Object чтобы не ловить в плагинах ClassNotFoundException
            Object entityInfo = ReflectionUtils.callDeclaredMethod(entityManager, "resolveEntityInfo", entityClass);
            return ReflectionUtils.callDeclaredMethod(entityInfo, "getName");
        }

        @SuppressWarnings("unchecked")
        private EntityProxy<T, Integer> getEntityProxy(T entity) {
            return ((EntityProxyAccessor) entity).getEntityProxy();
        }

        private String getPolymorphicName(String fieldName) {
            //без toUpperCase на POSTGRES не работает
            return CaseFormat.UPPER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, fieldName).toUpperCase();
        }

        private Method getAccessor(String fieldName) {
            return accessors.get(fieldName);
        }

        private Method getMutator(String fieldName) {
            return mutators.get(fieldName);
        }

    }

    protected ActiveObjects activeObjects;
    protected Class<T> entityClass;
    protected EntityInfo entityInfo;
    protected EntityCache<T> cache;

    public AbstractEntityDao(ActiveObjects activeObjects, Class<T> entityClass) {
        this.activeObjects = activeObjects;
        this.entityClass = entityClass;
        this.cache = new EntityCache<>();
    }

    @Override
    public T find(Integer id) {
        return getSingle(activeObjects.find(entityClass, "ID = ?", id));
    }

    @Override
    public List<T> findAll() {
        return Arrays.asList(activeObjects.find(entityClass));
    }

    @Override
    public List<T> findAll(Filter<T> filter) {
        return findAll().stream().filter(filter::matches).collect(Collectors.toList());
    }

    @Override
    public List<T> findAllLite() {
        List<T> result = new LinkedList<>();
        activeObjects.stream(entityClass, result::add);
        return result;
    }

    @Override
    public List<T> findAllLite(Filter<T> filter) {
        List<T> result = new LinkedList<>();
        activeObjects.stream(entityClass, entity -> {
            if (filter.matches(entity)) {
                result.add(entity);
            }
        });
        return result;
    }

    @Override
    public T createEmpty() {
        //todo починить The follow required fields were not set when trying to create entity 'com.bftcom.jira.sla.metrics.entity.Metric', those fields are: ImmutableFieldInfo{fieldName='KEY', polymorphicName='null', accessor=public abstract java.lang.String com.bftcom.jira.sla.metrics.entity.Metric.getKey(), mutator=public abstract void com.bftcom.jira.sla.metrics.entity.Metric.setKey(java.lang.String), primary=false, nullable=false, autoIncrement=false, defaultValue=false, fieldType=class java.lang.String, typeInfo=String:VARCHAR, generatorType=null}, ImmutableFieldInfo{fieldName='NAME', polymorphicName='null', accessor=public abstract java.lang.String com.bftcom.jira.sla.metrics.entity.Metric.getName(), mutator=public abstract void com.bftcom.jira.sla.metrics.entity.Metric.setName(java.lang.String), primary=false, nullable=false, autoIncrement=false, defaultValue=false, fieldType=class java.lang.String, typeInfo=String:VARCHAR, generatorType=null}
        return activeObjects.create(entityClass);
    }

    @Override
    public T create(T entity) {
        entity.save();
        return entity;
    }

    @Override
    public T update(T entity) {
        entity.save();
        return entity;
    }

    @Override
    public void delete(Integer id) {
        activeObjects.deleteWithSQL(entityClass, " ID = ? ", id);
    }

    @Override
    public void batchCreate(Collection<T> entities) throws SQLException, ReflectiveOperationException {
        if (entities == null || entities.size() == 0) return;
        initEntityInfo();
        EntityManager entityManager = entityInfo.getEntityManager();
        if (entityManager != null) {
            String table = entityInfo.getTableName();
            Map<String, TypeInfo<Integer>> fields = entityInfo.getFields();
            final DatabaseProvider provider = entityManager.getProvider();
            Connection conn = null;
            PreparedStatement stmt = null;
            try {
                conn = provider.getConnection();
                StringBuilder sql = new StringBuilder("INSERT INTO " + provider.withSchema(table) + "( ");
                for (String fieldName : fields.keySet()) {
                    sql.append(entityInfo.getDbFieldName(fieldName)).append(",");
                }
                if (sql.charAt(sql.length() - 1) == ',') {
                    sql.setLength(sql.length() - 1);
                }
                sql.append(") VALUES (");
                for (String ignored : fields.keySet()) {
                    sql.append("?,");
                }
                if (sql.charAt(sql.length() - 1) == ',') {
                    sql.setLength(sql.length() - 1);
                }
                sql.append(")");
                stmt = provider.preparedStatement(conn, sql);
                for (T entity : entities) {
                    fillPreparedStatementParams(entityManager, entityInfo, stmt, entity, false);
                    stmt.addBatch();
                }
                stmt.executeBatch();
            } finally {
                close(stmt);
                close(conn);
            }
        }
    }

    @Override
    public void batchUpdate(Collection<T> entities) throws SQLException, ReflectiveOperationException {
        if (entities == null || entities.size() == 0) return;
        initEntityInfo();
        EntityManager entityManager = entityInfo.getEntityManager();
        if (entityManager != null) {
            String table = entityInfo.getTableName();
            final DatabaseProvider provider = entityManager.getProvider();
            Connection conn = null;
            PreparedStatement stmt = null;
            try {
                conn = provider.getConnection();
                StringBuilder sql = new StringBuilder("UPDATE " + provider.withSchema(table) + " SET ");
                for (String fieldName : entityInfo.getFields().keySet()) {
                    sql.append(entityInfo.getDbFieldName(fieldName)).append(" = ?,");
                }
                if (sql.charAt(sql.length() - 1) == ',') {
                    sql.setLength(sql.length() - 1);
                }
                sql.append(" WHERE ").append(provider.processID("ID")).append(" = ?");
                stmt = provider.preparedStatement(conn, sql);
                for (T entity : entities) {
                    fillPreparedStatementParams(entityManager, entityInfo, stmt, entity, true);
                    stmt.addBatch();
                }
                stmt.executeBatch();
            } finally {
                close(stmt);
                close(conn);
            }
        }
    }

    @Override
    public void batchDelete(Collection<Integer> ids) throws SQLException {
        if (ids == null || ids.size() == 0) return;
        initEntityInfo();
        EntityManager entityManager = entityInfo.getEntityManager();
        if (entityManager != null) {
            String table = entityInfo.getTableName();
            final DatabaseProvider provider = entityManager.getProvider();
            Connection conn = null;
            PreparedStatement stmt = null;
            try {
                conn = provider.getConnection();
                StringBuilder sql = new StringBuilder("DELETE FROM " + provider.withSchema(table) + " WHERE " + provider.processID("ID") + " = ?");
                stmt = provider.preparedStatement(conn, sql);
                for (Integer id : ids) {
                    stmt.setInt(1, id);
                    stmt.addBatch();
                }
                stmt.executeBatch();
            } finally {
                close(stmt);
                close(conn);
            }
        }
    }

    @Override
    public EntityCache<T> createCache() {
        return new EntityCache<>();
    }

    @Override
    public EntityCache<T> getCache() {
        return cache;
    }

    @Override
    public void flush() throws SQLException, ReflectiveOperationException {
        flush(cache);
    }

    @Override
    public void flush(EntityCache<T> cache) throws SQLException, ReflectiveOperationException {
        batchCreate(cache.getCreatedEntities());
        batchUpdate(cache.getUpdatedEntities());
        batchDelete(cache.getDeletedEntities());
        cache.clear();
    }

    private void fillPreparedStatementParams(EntityManager entityManager, EntityInfo entityInfo, PreparedStatement ps, T entity, boolean updateQuery) throws SQLException, ReflectiveOperationException {
        int index = 1;
        for (String fieldName : entityInfo.getFields().keySet()) {
            Object value = getEntityValue(entity, fieldName);
            if (value == null) {
                entityManager.getProvider().putNull(ps, index++);
            } else {
                TypeInfo dbType = entityInfo.getFields().get(fieldName);
                Object dbValue = dbType.getLogicalType().validate(value);
                dbType.getLogicalType().putToDatabase(entityManager, ps, index++, dbValue, dbType.getJdbcWriteType());
            }
        }
        if (updateQuery) {
            TypeInfo pkType = entityInfo.getIdType();
            pkType.getLogicalType().putToDatabase(entityManager, ps, index, entity.getID(), pkType.getJdbcWriteType());
        }
    }

    private Object getEntityValue(T entity, String fieldName) throws InvocationTargetException, IllegalAccessException {
        Method getter = entityInfo.getAccessor(fieldName);
        return getter != null ? getter.invoke(entity) : null;
    }


    protected void initEntityInfo() {
        if (entityInfo == null) {
            activeObjects.executeInTransaction(() -> {
                T entity = getFakeEntity();
                try {
                    entityInfo = new EntityInfo(entityClass, entity);
                } catch (ReflectiveOperationException e) {
                    throw new RuntimeException("Failed to init entity info", e);
                } finally {
                    activeObjects.delete(entity);
                }
                return null;
            });

        }
    }

    protected static <T> String getInClauseOperand(T[] objects, Converter<T, String> converter) {
        return getInClauseOperand(Arrays.asList(objects), converter);
    }

    protected static <S> String getInClauseOperand(Collection<S> collection, Converter<S, String> converter) {
        if (collection == null || collection.size() == 0) {
            return "()";
        }
        Iterator<S> iterator = collection.iterator();
        S element = iterator.next();
        StringBuilder builder = new StringBuilder("(");
        builder.append(converter.convert(element));
        while (iterator.hasNext()) {
            element = iterator.next();
            builder.append(", ").append(converter.convert(element));
        }
        builder.append(")");
        return builder.toString();
    }

    protected void close(Statement statement) throws SQLException {
        statement.close();
    }

    protected void close(Connection connection) throws SQLException {
        connection.close();
    }

    protected void closeQuietly(Statement statement) {
        try {
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void closeQuietly(Connection connection) {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected T getSingle(T[] entities) {
        return entities != null && entities.length == 1 ? entities[0] : null;
    }

    //todo Это плохой способ построения ентити только ради парсинга ее класса. нужно выкрутиться через EntityManager
    protected abstract T getFakeEntity();

}
