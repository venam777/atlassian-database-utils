package com.venam.atlassian.database.utils.dao;

import com.atlassian.activeobjects.tx.Transactional;
import com.venam.atlassian.database.utils.tools.Filter;
import net.java.ao.Entity;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

/**
 *
 */
@Transactional
public interface EntityDao<T extends Entity> {

    /**
     * Ищет entity c указанным id
     * @param id id entity
     * @return найденная entity
     */
    T find(Integer id);

    List<T> findAll();

    List<T> findAll(Filter<T> filter);

    /**
     * Ищет все записи в таблицы, <strong>БЕЗ СВЯЗАННЫХ СУЩНОСТЕЙ</strong>, только простые поля
     * @return все записи из таблицы
     */
    List<T> findAllLite();

    List<T> findAllLite(Filter<T> filter);

    T createEmpty();

    T create(T entity);

    T update(T entity);

    void delete(Integer id);

    void batchCreate(Collection<T> entities) throws SQLException, ReflectiveOperationException;

    void batchUpdate(Collection<T> entities) throws SQLException, ReflectiveOperationException;

    void batchDelete(Collection<Integer> ids) throws SQLException, ReflectiveOperationException;

    /**
     * Создает новый кеш, который не присоединен к DAO, его можно передавать и наполнять не зависимо от DAO
     * @return кэш
     */
    EntityCache<T> createCache();

    /**
     * Получает кэш, который хранится внутри самого dao
     * @return кэш
     */
    EntityCache<T> getCache();

    /**
     * Сохраняет и обнуляет кеш, который хранится в DAO
     */
    void flush() throws SQLException, ReflectiveOperationException;

    /**
     * Сохраняет и обнуляет сторонний кэш
     * @param cache кэш
     */
    void flush(EntityCache<T> cache) throws SQLException, ReflectiveOperationException;

}
