package com.venam.atlassian.database.utils.tools;

public interface Filter<T> {

    boolean matches(T object);

}
