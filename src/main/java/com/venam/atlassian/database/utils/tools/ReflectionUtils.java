package com.venam.atlassian.database.utils.tools;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class ReflectionUtils {

    public static <RESULT, OBJECT> RESULT callDeclaredMethod(OBJECT object, String method, Object... params) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Class<?> clazz = object.getClass();
        Class<?>[] array = Arrays.stream(params).map(p -> p.getClass()).toArray(value -> new Class<?>[params.length]);
        Method m = clazz.getDeclaredMethod(method, array);
        m.setAccessible(true);
        return (RESULT) m.invoke(object, params);
    }

    public static <RESULT, OBJECT> RESULT getDeclaredVariable(OBJECT object, String variable) throws NoSuchFieldException, IllegalAccessException {
       return getDeclaredVariable(object.getClass(), object, variable);
    }

    public static <RESULT, OBJECT> RESULT getDeclaredVariable(Class<? extends OBJECT> clazz, OBJECT object, String variable) throws NoSuchFieldException, IllegalAccessException {
        Field field = clazz.getDeclaredField(variable);
        field.setAccessible(true);
        return (RESULT) field.get(object);
    }
}
