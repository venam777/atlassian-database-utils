package com.venam.atlassian.database.utils.tools;

public interface Converter<FROM, TO> {

    TO convert(FROM value);

}
