package com.venam.atlassian.database.utils.dto;

import net.java.ao.Entity;
import net.java.ao.EntityManager;
import net.java.ao.RawEntity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.beans.PropertyChangeListener;

/**
 *
 */
@XmlRootElement
public abstract class EntityDto<SELF extends Entity> implements Entity {

    @XmlElement
    protected Integer id;

    public EntityDto() {}

    public EntityDto(Integer id) {
        this.id = id;
    }

    public EntityDto(Entity entity) {
        this.id = entity.getID();
    }

    public void setID(Integer id) {
        this.id = id;
    }

    @Override
    public int getID() {
        return id;
    }

    @Override
    public void init() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void save() {
        throw new UnsupportedOperationException();
    }

    @Override
    public EntityManager getEntityManager() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        throw new UnsupportedOperationException();
    }

    @Override
    public <X extends RawEntity<Integer>> Class<X> getEntityType() {
        return (Class<X>) getEntityClass();
    }

    /**
     * Заполняет поля домена entity из объекта dto
     * @param entity домен, полученный с помощью activeObjects.create или activeObjects.found
     * @return домен entity, Заполненный значениями полей из dto
     */
    public abstract SELF toEntity(SELF entity);

    protected abstract Class<SELF> getEntityClass();
}
